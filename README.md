# Rohsa's Workshop

The source code of my website!

## Development

``` text
npm install --no-bin-links
npm run dev
```

Fedora with openssl 3.0 may break nodejs (ERR_OSSL_EVP_UNSUPPORTED error), use external nodejs can resolve it:

``` text
sudo npm install -g n
sudo n lts
```

## Generate Static Files

``` text
npm run generate
```

## License

The MIT License.

© 2022 Rohsa All rights reserved. 

