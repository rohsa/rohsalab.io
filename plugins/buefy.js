import Vue from 'vue'
import { Input, Select, Upload, Loading } from 'buefy'

Vue.use(Input)
Vue.use(Select)
Vue.use(Upload)
Vue.use(Loading)

