import Vue from "vue";

Vue.prototype.$replaceQuery = function (newQuery) {
    const query = JSON.parse(JSON.stringify(this.$route.query))
    Object.keys(newQuery).forEach(k => { query[k] = newQuery[k] })
    this.$router.replace({ query }).catch(()=>{})
}
